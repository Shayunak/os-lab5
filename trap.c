#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "x86.h"
#include "traps.h"
#include "spinlock.h"


// Interrupt descriptor table (shared by all CPUs).
struct gatedesc idt[256];
extern uint vectors[];  // in vectors.S: array of 256 entry pointers
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);

  initlock(&tickslock, "time");
}

void
idtinit(void)
{
  lidt(idt, sizeof(idt));
}

//LAB 5
int
pageFaultHandler(void)
{
  struct proc *p = myproc();
  uint addr = rcr2();
  uint newIndex = 0;
  //check validate
  int i = 0;
  for (i = 0; i < 16; i++)
  {
    if ( (p->files[i].valid) && (addr >= p->files[i].startAddr) && (addr < (p->files[i].startAddr + p->files[i].length)) ){
      break;
    }
  }

  if (i == 16)
  {
    return 0;
  }
  
  // allocation + mapping
  newIndex = (addr - p->files[i].startAddr) / PGSIZE;
  char *mem;
  mem = kalloc();
  memset(mem, 0, PGSIZE);

  if(mappages(p->pgdir, (char*)(p->files[i].startAddr + newIndex * PGSIZE), PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
    cprintf("access denided!\n");
    kfree(mem);
    return 0;
  }
  
  // read from file
  if (p->ofile[p->files[i].fd] == 0)
  {
    cprintf("file is not opened!\n");
    return 0;
  }

  if (mmapRead(p->ofile[p->files[i].fd], (char*)(p->files[i].startAddr + newIndex * PGSIZE), newIndex * PGSIZE, PGSIZE) < 0){
    cprintf("panirrrr\n");
  }

  if (!p->files[i].write)
  {
    if(changeMapPerm(p->pgdir, (char*)(p->files[i].startAddr + newIndex * PGSIZE), PGSIZE, V2P(mem), PTE_U) < 0){
      cprintf("changePerm failed\n");
    }
  }

  cprintf("addr: %x\n", addr);
  return 1;
}

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
  if(tf->trapno == T_SYSCALL){
    if(myproc()->killed)
      exit();
    myproc()->tf = tf;
    syscall();
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
  case T_IRQ0 + IRQ_TIMER:
    if(cpuid() == 0){
      acquire(&tickslock);
      ticks++;
      wakeup(&ticks);
      release(&tickslock);
    }
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE:
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_COM1:
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
            cpuid(), tf->cs, tf->eip);
    lapiceoi();
    break;
    
  ///LAB 5
  case T_PGFLT:
    if (pageFaultHandler()){
      break;
    }
    

  //PAGEBREAK: 13
  default:
    if(myproc() == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpuid(), tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
            "eip 0x%x addr 0x%x--kill proc\n",
            myproc()->pid, myproc()->name, tf->trapno,
            tf->err, cpuid(), tf->eip, rcr2());
    myproc()->killed = 1;
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING &&
     tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
    exit();
}
