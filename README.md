XV6-Modified
======

XV6-Modified is an OS cloned from [xv6 kernel](https://github.com/mit-pdos/xv6-public) with some new features . xv6 is a re-implementation of Dennis Ritchie's and Ken Thompson's Unix
Version 6 (v6). 
- [POS](#pos)
	- [New Features](#new-features)
		- [Part 1 (Introduction to XV6):](#part-1-introduction-to-xv6)
		- [Part 2 (System Calls) :](#part-2-system-calls)
		- [Part 3 (CPU Scheduling):](#part-3-cpu-scheduling)
		- [Part 4 (Synchronization And Concurrency):](#part-4-synchronization-and-concurrency)
		- [Part 5 (Memmory Management):](#part-5-memory-management)
	- [How to use?](#how-to-use)


New Features 
------
These new features are added in different project repositories you can check them out in each checkout link provided for each part


### Part 1 (Introduction to XV6):
* Printing our names on the shell on start up of system.
* Added Ctrl+v, Ctrl+b, Ctrl+c, and Ctrl+x shortcuts.
	* Ctrl+v: for pasting contents of the buffer on the shell
	* Ctrl+b: for copying the selected content and then 			pasting the former content of the buffer on the shell
	* Ctrl+c: copying the selected content on the shell to the buffer
	* Ctrl+x: cutting the selected content on the shell to the buffer
* A user program named `lcm` or least common multiplicant added.

-> [check it out](https://gitlab.com/SMahdiHosseini/os-lab-1)
### Part 2 (System Calls) :
* Added some system calls
	* sys_reverse_number: A system call that reverses a number passed to it
	* sys_print_trace: A system call that returns a list of system calls
	* sys_get_children: A system call that returns the list  of child process belonging to a process that its id is passed as an argument
	* sys_trace_syscalls: A system call that traces each system call and saves number of calls for each system call

-> [check it out](https://bitbucket.org/Shayunak/os_computer_assignment_lab_2/src/master/)
### Part 3 (CPU Scheduling):
* adding a multi-level feedback scheduler including : 
	* Added multiple queue scheduling
  * Added RR, Lottery, and BJF algorithms
  * Added the concept of aging

-> [check it out](https://gitlab.com/soheilzi/os-lab3)
### Part 4 (Synchronization And Concurrency):
* Implemented semaphores and all related synchronization functions
* Implemented readers writers problem with the semaphores that we created

-> [check it out](https://gitlab.com/Shayunak/os-lab4)
### Part 5 (memory management): 
* Added kalloc system call
* Added mappages systemcall (similar to POSIX mmap)

-> [check it out](https://gitlab.com/Shayunak/os-lab5)

How to use? 
------

you can make this kernel using `make` command.
also you can run this kernel on qemu virtual machine using `make qemu` command.

