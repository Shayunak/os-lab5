#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main()
{
    // char* f = (char*)mmap(0, 100, 1, 1, 1, 0);
    int fd;
    if((fd = open("lab5_test.txt", O_CREATE|O_WRONLY)) < 0){
      printf(1, "cannot open lab5_test.txt\n");
      exit();
    }
    
    char temp[120] = "0123456789abcdefgh\nLAB-5 OS\nShayan Hamidi Dehshali 810197493\nSoheil Zibakhsh 810197655\nMahdi Hosseini 810197652\n";
    
    write(fd, temp, 120);
    printf(1, "free pages: %d\n", get_free_pages_count());
    char* f2 = (char*)mmap(0, 100, 1, 1, fd, 0);
    printf(1, "free pages: %d\n", get_free_pages_count());
    printf(1, "first address: %c\n", f2[99]);
    printf(1, "second address: %c\n", f2[80]);
    printf(1, "free pages: %d\n", get_free_pages_count());
    f2[10] = 'F';

    close(fd);
    exit();
}