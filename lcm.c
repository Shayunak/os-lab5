#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define MAX_NUM_SUPPORT 8
#define I(c)  ((c)-'0') //character to integer conversion
#define S(i)  ((i) + '0') //integer to character conversion

//number of digits of an integer number
int intLenght(int n){
  int i;

  for(i = 1; (n = n / 10) ; i++);
  return i;
}

//string to integer conversion
int stoi(const char *s){
  int value = 0;
  int n;
  for(n = 0; s[n]; n++)
    value = (value * 10 + I(s[n]) );

  return value;
}

//integer to string conversion
void itos(char* output , int input){
    int i,d;
    int n = intLenght(input);
    for(i = 1 ; i <= n ; i++){
        d = input % 10;
        input = (input - d) / 10;
        output[n-i] = S(d);
    }
}

//core calculation for lcm
//input:an array of numbers         //output:lcm
int lcm_calculation(int numbers[] , int n){
    int i , lcm , isLcm;
    int maximumNumber = numbers[0];
    for(i = 0 ; i < n ; i++){
        if(numbers[i] > maximumNumber)
            maximumNumber = numbers[i];
    }

    for(lcm = maximumNumber ; ; lcm++){
        isLcm = 1;
        for(i = 0 ; i < n ; i++){
            if(lcm % numbers[i] != 0){
                isLcm = 0;
                break;
            }
        }
        if(isLcm)
            return lcm;
    }
}

int main(int argc, char *argv[]){
	int numbers[MAX_NUM_SUPPORT];
	int n,result,fd;
	if(argc < 2){
		printf(1 , "lcm: Not Enough arguments.\n");
		exit();
	}
	if(argc > MAX_NUM_SUPPORT + 1){
		printf(1 , "lcm: Too many arguments.At most %d arguments is supported.\n" , MAX_NUM_SUPPORT);
		exit();
	}
	for(n = 1 ; n < argc ; n++)
		numbers[n - 1] = stoi(argv[n]);
	
    result = lcm_calculation(numbers , argc - 1);
    //file oppening for the output
    if((fd = open("lcm_result.txt", O_CREATE|O_WRONLY)) < 0){
      printf(1, "lcm: cannot open lcm_result.txt\n");
      exit();
    }
    //writing in the file after converting integer value to string
    char stringOutput[30];
    stringOutput[29] = '\n';
    itos(stringOutput , result);
    if (write(fd, stringOutput,30) != 30) {
      printf(1, "lcm: write error\n");
      exit();
    }

    close(fd);
    exit();
}
